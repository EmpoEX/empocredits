@ECHO OFF
cls
TITLE Clean Build
ECHO Cleaning folders...
del build /Q /F
del Makefile /Q /F
del Makefile.Release /Q /F 
del Makefile.Debug /Q /F
del object_script.bananabits-qt.Debug /Q /F
del object_script.bananabits-qt.Release /Q /F
ECHO Cleaned!
PAUSE
