
EmpoCredits development tree

EmpoCredits is a PoS-Only cryptocurrency.

EmpoCredits is dependent upon libsecp256k1.

Block Spacing:  60 Seconds
Stake Minimum Age: 4 Hours

Port: 31769
RPC Port: 31770

EmpoCredits includes an Address Index feature, based on the address index API (searchrawtransactions RPC command) implemented in Bitcoin Core but modified implementation to work with the EmpoCredits codebase (PoS coins maintain a txindex by default for instance).

Initialize the Address Index By Running with -reindexaddr Command Line Argument.  It may take 10-15 minutes to build the initial index.


COMPILE INSTRUCTIONS

Compile and install libsecp256k1.

cd secp256k1

./autogen.sh

./configure

make

sudo make install

cd ../

Compile and install EmpoCreditsd.

cd src

cd leveldb

chmod +x build_detect_platform && make clean && make libleveldb.a libmemenv.a

cd ../

make -f makefile.unix USE_UPNP=- STATIC=1